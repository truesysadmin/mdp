#!/bin/bash

set -o errexit -o pipefail -o noclobber -o nounset

cd /sunflower

wget https://gitlab.com/white-sagittarius/sunflower/-/raw/main/sunflower.ipynb
./archive.sh "${count:-5}" sunflower.ipynb upload-to-google-drive f

if [ -d /output/upload-to-google-drive ]; then
    rm -r /output/upload-to-google-drive
fi

cp -a upload-to-google-drive/ /output/
